#ifndef PRINT_H
#define PRINT_H

void mov_cursor();
void print_char(char c);
void print(char *str);
void clear_screen(void);

#endif